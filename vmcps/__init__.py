#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""VMCPS package."""

from .assistant import Assistant
from .configuration import Configuration

__all__ = [
  "Assistant",
  "Configuration"
]
