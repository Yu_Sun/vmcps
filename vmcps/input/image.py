#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""``image`` module of ``input`` package."""

from typing import TypeVar
from os.path import exists
# Pillow
from PIL import (
    Image as PILImage,
    UnidentifiedImageError
)
# Numpy
from numpy import (
    ndarray,
    asarray
)
# OpenCV
from cv2 import (  # pylint: disable=no-name-in-module
    cvtColor,
    COLOR_RGB2BGR
)
from .typing import InputType

ImageT = TypeVar("ImageT", bound="Image")


class Image(InputType):
    """Image input."""

    @property
    def is_open(self: ImageT) -> bool:
        """bool: Returns whether it's currently open."""
        return self._opened

    @property
    def end_of_data(self: ImageT) -> bool:
        """bool: Returns whether the input data reached its end."""
        return self._end_of_file

    _file: PILImage

    def __init__(
        self: ImageT,
        filename: str
    ) -> None:
        """Create image.

        Args:
            filename (str):
                Device number.

        """
        self._opened = False
        self._end_of_file = True
        self._filename = filename

    def open(self: ImageT) -> ImageT:
        """Open file.

        Returns:
            InputT:
                Instance.

        Raises:
            FileNotFoundError:
                If the file is not found.
            IOError:
                If the file format is unknown or invalid.

        """
        if not self.is_open:
            if exists(self._filename):
                try:
                    self._file = PILImage.open(self._filename)
                except UnidentifiedImageError as exception:
                    raise IOError(
                        f"File '{self._filename}' format unknown or invalid."
                    ) from exception
                self._opened = True
                self._end_of_file = False
            else:
                raise FileNotFoundError(f"File '{self._filename}' not found.")
        return self

    def read(self: ImageT) -> ndarray:
        """Return image data.

        Returns:
            ndarray:
                Image data (BGR pixel pattern).

        Raises:
            IOError:
                If image is not opened.

        """
        if self.is_open:
            data = cvtColor(asarray(self._file), COLOR_RGB2BGR)
            self._end_of_file = True
            return data
        else:
            raise IOError(f"Image {self._filename} not opened.")

    def close(self: ImageT) -> None:
        """Close file."""
        if self.is_open:
            self._end_of_file = True
            self._file.close()
            self._opened = False
