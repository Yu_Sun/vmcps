#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: AGPL-3.0-or-later

"""VMCP support for ROMP."""

from typing import ClassVar
from math import radians
# Numpy
from numpy import ndarray
# ROMP
from romp import ROMP
from romp.main import default_settings as romp_default_settings
# OSC protocol layer
from vmcp.osc import OSC
from vmcp.osc.typing import Message
from vmcp.osc.backend.osc4py3 import as_eventloop as backend
# VMC protocol layer
from vmcp.typing import (
    CoordinateVector,
    Quaternion,
    Scale,
    Bone,
    Timestamp
)
from vmcp.protocol import (
    root_transform,
    bone_transform,
    time
)
# VMCPS
from vmcps.input.typing import InputType
from vmcps.input import (
    Image,
    Camera
)
from vmcps import (
    Configuration,
    Assistant
)


# VMCP logic
class ROMPAssistant(Assistant):
    """Concrete ROMP assistant."""

    BONES: ClassVar[tuple] = (
        Bone.HIPS,  # Pelvis
        Bone.LEFT_UPPER_LEG,  # L_Hip
        Bone.RIGHT_UPPER_LEG,  # R_Hip
        Bone.SPINE,  # SPINE1
        Bone.LEFT_LOWER_LEG,  # L_Knee
        Bone.RIGHT_LOWER_LEG,  # R_Knee
        Bone.CHEST,  # SPINE2
        Bone.LEFT_FOOT,  # L_Ankle
        Bone.RIGHT_FOOT,  # R_Ankle
        Bone.UPPER_CHEST,  # SPINE3
        Bone.LEFT_TOES,  # L_Foot
        Bone.RIGHT_TOES,  # R_Foot
        Bone.NECK,  # NECK
        Bone.LEFT_SHOULDER,  # L_Collar
        Bone.RIGHT_SHOULDER,  # R_Collar
        Bone.HEAD,  # HEAD
        Bone.LEFT_UPPER_ARM,  # L_Shoulder
        Bone.RIGHT_UPPER_ARM,  # R_Shoulder
        Bone.LEFT_LOWER_ARM,  # L_Elbow
        Bone.RIGHT_LOWER_ARM,  # R_Elbow
        Bone.LEFT_HAND,  # L_Wrist
        Bone.RIGHT_HAND,  # R_Wrist
        Bone.LEFT_MIDDLE_PROXIMAL,  # L_Hand
        Bone.RIGHT_MIDDLE_PROXIMAL  # R_hand
    )

    _romp: ROMP
    """ROMP: Instance."""

    def open(self) -> "ROMPAssistant":
        """Start.

        Returns:
            AssistantClass:
                Instance.

        """
        if not self._opened:
            config = romp_default_settings
            config.GPU = self._configuration["gpu"]
            config.onnx = self._configuration["onnx"]
            config.temporal_optimize = self._configuration["temporal-optimize"]
            config.smooth_coeff = self._configuration["smooth-coeff"]
            self._romp = ROMP(config)
            self._channel.open()
            self._opened = True
        return self

    def process(
        self,
        data: ndarray
    ) -> None:
        """Process given input.

        Args:
            data (ndarray):
                Input.

        """
        result = self._romp(data)
        if result is not None:  # At least 1 person detected.
            root_position = CoordinateVector(
                -result["cam_trans"][self._configuration["subject"]][0],
                -result["cam_trans"][self._configuration["subject"]][1],
                -result["cam_trans"][self._configuration["subject"]][2]
            )
            root_rotation = Quaternion.from_euler(radians(-180), 0, 0)
            scale = 1 - result["smpl_betas"][self._configuration["subject"]][0]
            if self._configuration["legacy"]:
                # Backward compatibility for VMC Protocol v2.0.0
                messages = [
                    Message(*root_transform(
                        CoordinateVector(
                            root_position.x,
                            root_position.y + scale + 1,
                            root_position.z + scale + 1
                        ),
                        root_rotation
                    ))
                ]
            else:
                # Since VMC Protocol v2.1.0
                messages = [
                    Message(*root_transform(
                        root_position,
                        root_rotation,
                        # Optional alignment
                        Scale(scale),
                        offset=CoordinateVector(0, -(scale + 1), -(scale + 1))
                    ))
                ]
            for index, rotation in enumerate(
                result["smpl_thetas"][
                    self._configuration["subject"]
                ].reshape(24, 3)  # 24 SMPL joints
            ):
                messages += [
                    Message(*bone_transform(
                        ROMPAssistant.BONES[index],
                        CoordinateVector.identity(),
                        Quaternion.from_rotvec(
                            -rotation[0],
                            rotation[1],
                            rotation[2]
                        ).conjugate()
                    ))
                ]
            messages += [
                Message(*time(
                    Timestamp(self._configuration["started-at"]).elapsed()
                ))
            ]
            self._channel.send(messages)
            self._channel.system.run()

    def close(self) -> None:
        """Terminate."""
        if self.is_open:
            self._channel.close()
            self._opened = False


# Runtime
configuration = Configuration("romp.yml")
source_type = configuration["source-type"]
source: InputType | None
match source_type:
    case "image":
        source = Image(configuration["source"])
    case "camera":
        source = Camera(configuration["source"])
    case _:
        source = None  # pylint: disable=invalid-name
if source is None:
    raise ValueError(f"Invalid source type: {source_type}")
else:
    osc = OSC(backend)
    assistant = ROMPAssistant(configuration, osc)
    try:
        with source.open():
            with osc.open():
                with assistant.open():
                    while not source.end_of_data:
                        assistant.process(source.read())
    except KeyboardInterrupt:
        print("Canceled.")
    finally:
        assistant.close()
        osc.close()
        source.close()
